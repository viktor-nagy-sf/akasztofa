package hu.silverfrog.akasztofa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
public class AkasztofaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AkasztofaApplication.class, args);
    }

}
