package hu.silverfrog.akasztofa.persistence.repository;

import hu.silverfrog.akasztofa.persistence.entity.GuessableWord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GuessableWordRepository extends JpaRepository<GuessableWord, Integer> {
    Page<GuessableWord> findByContentLikeIgnoreCase(String filter, Pageable request);
    long countByContentLikeIgnoreCase(String filter);
    GuessableWord findFirstByContent(String content);
}
