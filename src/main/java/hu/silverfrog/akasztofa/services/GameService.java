package hu.silverfrog.akasztofa.services;

import hu.silverfrog.akasztofa.persistence.entity.GuessableWord;
import hu.silverfrog.akasztofa.persistence.repository.GuessableWordRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class GameService {
    private final GuessableWordRepository repository;

    public GameService(GuessableWordRepository repository) {
        this.repository = repository;
    }

    public String getRandomWord() {
        List<GuessableWord> guessableWords = repository.findAll();
        Random generator = new Random();
        return guessableWords.get(generator.nextInt(guessableWords.size())).getContent();
    }

    public void save(GuessableWord item) {
        try {
            repository.save(item);
        } catch (DataIntegrityViolationException e) {
            throw new GuessableWordAlreadyExistsException(item, e);
        }
    }

    public Page<GuessableWord> fetchGuessableWords(Optional<String> optionalFilter, Pageable request) {
        return repository.findByContentLikeIgnoreCase(wrapOptionalFilter(optionalFilter), request);
    }


    public int countAnyMatching(Optional<String> optionalFilter) {
        return (int) repository.countByContentLikeIgnoreCase(wrapOptionalFilter(optionalFilter));
    }

    private String wrapOptionalFilter(Optional<String> filter) {
        return "%" + filter.orElse("") + "%";
    }

    @Transactional(rollbackOn = GuessableWordAlreadyExistsException.class)
    public void saveBatch(List<GuessableWord> guessableWords) {
        guessableWords.forEach(this::save);
    }

    public static class GuessableWordAlreadyExistsException extends RuntimeException {
        private final GuessableWord guessableWord;

        public GuessableWordAlreadyExistsException(GuessableWord guessableWord, DataIntegrityViolationException e) {
            super(e);
            this.guessableWord = guessableWord;
        }

        public GuessableWord getItem() {
            return guessableWord;
        }
    }
}
