package hu.silverfrog.akasztofa.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import hu.silverfrog.akasztofa.views.game.GameComponent;

@Route("")
public class Main extends VerticalLayout {

    public Main() {
        setAlignItems(Alignment.CENTER);
        add(new H2("Üdvözöllek az Akasztófa játékban!"));
        var button = new Button("Játék megnyitása");
        button.addClickListener(e -> UI.getCurrent().navigate(GameComponent.class));
        add(button);
    }
}