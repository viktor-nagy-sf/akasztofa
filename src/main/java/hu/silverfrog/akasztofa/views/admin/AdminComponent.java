package hu.silverfrog.akasztofa.views.admin;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import hu.silverfrog.akasztofa.services.GameService;

@Route("/admin")
public class AdminComponent extends VerticalLayout {

    public AdminComponent(GameService service, GuessableWordDataProviderFactory dataProviderFactory) {
        var view = new DefaultAdminView(dataProviderFactory);
        new AdminPresenter(view, service);
        setSizeFull();
        add(view);
    }

}
