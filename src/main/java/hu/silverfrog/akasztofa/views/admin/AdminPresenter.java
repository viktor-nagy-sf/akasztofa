package hu.silverfrog.akasztofa.views.admin;

import com.opencsv.bean.CsvToBeanBuilder;
import hu.silverfrog.akasztofa.persistence.entity.GuessableWord;
import hu.silverfrog.akasztofa.services.GameService;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class AdminPresenter implements AdminView.AdminViewListener {
    private final AdminView view;
    private final GameService service;

    public AdminPresenter(AdminView view, GameService service) {
        this.service = service;
        this.view = view;
        view.addListener(this);
    }

    @Override
    public void saveItem(GuessableWord item) {
        service.save(item);
        view.updateItem(item);
    }

    @Override
    public void processUpload(InputStream stream) {
        view.clearImportResultMessage();
        List<GuessableWord> words = new CsvToBeanBuilder<GuessableWord>(new InputStreamReader(stream)).withType(GuessableWord.class).build().parse();
        try {
            service.saveBatch(words);
            view.displayImportSuccessMessage(words.size());
            view.updateItems();
        } catch (GameService.GuessableWordAlreadyExistsException e) {
            var duplicateElementIndex = words.indexOf(e.getItem()) + 1;
            view.displayImportDuplicateErrorMessage(duplicateElementIndex, e.getItem().getContent());
        }
    }

}
