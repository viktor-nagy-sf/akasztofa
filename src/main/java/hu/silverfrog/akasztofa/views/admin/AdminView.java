package hu.silverfrog.akasztofa.views.admin;

import com.vaadin.flow.data.provider.DataProvider;
import hu.silverfrog.akasztofa.persistence.entity.GuessableWord;

import java.io.InputStream;

public interface AdminView {

    void addListener(AdminViewListener listener);
    void updateItems();
    void displayImportDuplicateErrorMessage(int lineNumber, String word);
    void displayImportSuccessMessage(int importCount);
    void clearImportResultMessage();

    void updateItem(GuessableWord item);

    interface AdminViewListener {
        void saveItem(GuessableWord item);
        void processUpload(InputStream stream);
    }

    interface DataProviderFactory {
        DataProvider<GuessableWord, String> createDataProvider();
    }
}
