package hu.silverfrog.akasztofa.views.admin;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.upload.Upload;
import elemental.json.Json;
import hu.silverfrog.akasztofa.persistence.entity.GuessableWord;
import hu.silverfrog.akasztofa.views.admin.builder.CsvFileUploadBuilder;
import hu.silverfrog.akasztofa.views.admin.builder.WordGridBuilder;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DefaultAdminView extends VerticalLayout implements AdminView {

    private final GuessableWordEditDialog editDialog;
    private final List<AdminViewListener> listeners = new ArrayList<>();
    private final Span uploadResultMessageContainer = new Span();
    private final Upload upload;

    private final Grid<GuessableWord> wordGrid;

    public DefaultAdminView(DataProviderFactory dataProviderFactory) {
        setSizeFull();
        var buttonLayout = new HorizontalLayout();
        buttonLayout.add(new Button("Új szó hozzáadása", buttonClickEvent -> openNewDialog()));
        upload = new CsvFileUploadBuilder().withRejectCallback(this::displayFileErrorMessage).withSuccessCallback(this::processStream).build();
        buttonLayout.add(upload);
        uploadResultMessageContainer.getStyle().set("padding-top", "10px");
        buttonLayout.add(uploadResultMessageContainer);
        add(buttonLayout);

        wordGrid = new WordGridBuilder()
                .withEditAction(this::openEditDialog)
                .withDataProvider(dataProviderFactory.createDataProvider())
                .build();
        add(wordGrid);

        editDialog = new GuessableWordEditDialog(this::saveItem);
        add(editDialog);
    }

    private void displayFileErrorMessage(String message) {
        uploadResultMessageContainer.setText(message);
    }

    @Override
    public void addListener(AdminViewListener listener) {
        listeners.add(listener);
    }

    @Override
    public void updateItems() {
        wordGrid.getDataProvider().refreshAll();
    }

    @Override
    public void displayImportDuplicateErrorMessage(int lineNumber, String word) {
        uploadResultMessageContainer.setText("Sikertelen import: " + lineNumber + ". sor - \"" + word + "\" - adatbázisban vagy a listában már létező szó!");
    }

    @Override
    public void displayImportSuccessMessage(int importCount) {
        uploadResultMessageContainer.setText("Sikeres import, importált szavak száma: " + importCount);
    }

    @Override
    public void clearImportResultMessage() {
        uploadResultMessageContainer.setText("");
    }

    @Override
    public void updateItem(GuessableWord item) {
        wordGrid.getDataProvider().refreshItem(item);
    }

    private void openEditDialog(GuessableWord item) {
        editDialog.open("Szó szerkesztése", item);
    }

    private void openNewDialog() {
        editDialog.open("Új szó hozzáadása", new GuessableWord());
    }

    private void saveItem(GuessableWord item) {
        listeners.forEach(listener -> listener.saveItem(item));
    }

    private void processStream(InputStream stream) {
        listeners.forEach(listener -> listener.processUpload(stream));
        //remove list of uploaded files displayed by default under the component
        upload.getElement().setPropertyJson("files", Json.createArray());
    }


}
