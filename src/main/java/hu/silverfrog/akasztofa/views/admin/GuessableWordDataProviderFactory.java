package hu.silverfrog.akasztofa.views.admin;

import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.QuerySortOrder;
import com.vaadin.flow.data.provider.SortDirection;
import hu.silverfrog.akasztofa.persistence.entity.GuessableWord;
import hu.silverfrog.akasztofa.services.GameService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.vaadin.artur.spring.dataprovider.FilterablePageableDataProvider;

import java.util.ArrayList;
import java.util.List;

@Component
public class GuessableWordDataProviderFactory implements AdminView.DataProviderFactory {
    private final GameService service;

    public GuessableWordDataProviderFactory(GameService service) {
        this.service = service;

    }

    @Override
    public DataProvider<GuessableWord, String> createDataProvider() {
        return new FilterablePageableDataProvider<>() {
            @Override
            protected Page<GuessableWord> fetchFromBackEnd(Query<GuessableWord, String> query, Pageable pageable) {
                return service.fetchGuessableWords(getOptionalFilter(), pageable);
            }

            @Override
            protected int sizeInBackEnd(Query<GuessableWord, String> query) {
                return service.countAnyMatching(getOptionalFilter());
            }

            @Override
            protected List<QuerySortOrder> getDefaultSortOrders() {
                List<QuerySortOrder> sortOrders = new ArrayList<>();
                sortOrders.add(new QuerySortOrder("id", SortDirection.ASCENDING));
                return sortOrders;
            }


        };
    }
}
