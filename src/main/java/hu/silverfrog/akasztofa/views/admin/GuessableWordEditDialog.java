package hu.silverfrog.akasztofa.views.admin;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import hu.silverfrog.akasztofa.persistence.entity.GuessableWord;
import hu.silverfrog.akasztofa.views.admin.builder.EditDialogLayoutBuilder;

import java.util.function.Consumer;

@CssImport(value = "./styles/shared-styles.css", themeFor = "vaadin-dialog-overlay")
public class GuessableWordEditDialog extends Dialog {
    private final Span headerSpan = new Span("");
    private final Consumer<GuessableWord> saveCallback;
    private final Binder<GuessableWord> binder = new Binder<>();

    private GuessableWord editedItem;

    public GuessableWordEditDialog(Consumer<GuessableWord> saveCallback) {
        this.saveCallback = saveCallback;
        setWidth("400px");
        TextField textField = new TextField("Szó");
        textField.setWidthFull();
        textField.addKeyDownListener(Key.ENTER, keyDownEvent -> save());
        var layoutBuilder = new EditDialogLayoutBuilder(this::close, this::save)
                .withHeaderText(headerSpan)
                .withContent(textField);
        add(layoutBuilder.build());
        binder.forField(textField).withValidator(content -> content.length() > 0, "Legalább 1 darab karaktert meg kell adni.")
                .bind(GuessableWord::getContent, GuessableWord::setContent);
    }

    public void open(String headerText, GuessableWord item) {
        this.headerSpan.setText(headerText);
        editedItem = item;
        binder.readBean(item);
        super.open();
    }

    private void save() {
        binder.validate();
        if (binder.isValid()) {
            binder.writeBeanIfValid(editedItem);
            saveCallback.accept(editedItem);
            close();
        }
    }
}
