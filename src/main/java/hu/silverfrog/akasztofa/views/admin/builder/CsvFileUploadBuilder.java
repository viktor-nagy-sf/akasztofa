package hu.silverfrog.akasztofa.views.admin.builder;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;

import java.io.InputStream;
import java.util.function.Consumer;

public class CsvFileUploadBuilder {
    private Consumer<InputStream> successCallback;
    private Consumer<String> rejectCallback ;

    public CsvFileUploadBuilder withSuccessCallback(Consumer<InputStream> successCallback) {
        this.successCallback = successCallback;
        return this;
    }

    public CsvFileUploadBuilder withRejectCallback(Consumer<String> rejectCallback) {
        this.rejectCallback = rejectCallback;
        return this;
    }

    public Upload build() {
        MemoryBuffer buffer = new MemoryBuffer();
        var upload = new Upload(buffer);
        upload.setUploadButton(new Button("Importálás"));
        upload.setAutoUpload(true);
        upload.setDropAllowed(false);
        upload.setAcceptedFileTypes("text/csv", ".csv");
        upload.setMaxFileSize(20971520);
        upload.addSucceededListener(event ->successCallback.accept(buffer.getInputStream()));
        upload.addFileRejectedListener(event->rejectCallback.accept(event.getErrorMessage()));
        return upload;
    }
}
