package hu.silverfrog.akasztofa.views.admin.builder;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class EditDialogLayoutBuilder {
    private static final String HEADER_FOOTER_BACKGROUND_COLOR = "#f3f5f7";

    private final Runnable cancelCallback;
    private final Runnable saveCallback;

    private Span headerSpan = new Span("");
    private Component content = new VerticalLayout();

    public EditDialogLayoutBuilder(Runnable cancelCallback, Runnable saveCallback) {
        this.cancelCallback = cancelCallback;
        this.saveCallback = saveCallback;
    }

    public EditDialogLayoutBuilder withHeaderText(Span headerSpan) {
        this.headerSpan = headerSpan;
        return this;
    }

    public EditDialogLayoutBuilder withContent(Component content) {
        this.content = content;
        return this;
    }

    public VerticalLayout build() {
        var layout = new VerticalLayout();
        layout.setMargin(false);
        layout.setPadding(false);

        headerSpan.getStyle().set("font-weight", "bold").set("font-size", "110%");

        var headerLayout = new HorizontalLayout();
        headerLayout.setWidthFull();
        headerLayout.setPadding(true);
        headerLayout.getStyle().set("background-color", HEADER_FOOTER_BACKGROUND_COLOR);
        headerLayout.add(headerSpan);
        layout.add(headerLayout);

        var contentLayout = new HorizontalLayout();
        contentLayout.setWidthFull();
        contentLayout.setPadding(true);
        contentLayout.add(content);
        layout.add(contentLayout);

        var buttonLayout = new HorizontalLayout();
        buttonLayout.setWidthFull();
        buttonLayout.setPadding(true);
        buttonLayout.getStyle().set("background-color", HEADER_FOOTER_BACKGROUND_COLOR);
        buttonLayout.setAlignItems(FlexComponent.Alignment.END);
        buttonLayout.add(new Button("Mégse", buttonClickEvent -> cancelCallback.run()));

        var saveButton = new Button("Mentés", buttonClickEvent -> saveCallback.run());
        saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        saveButton.getStyle().set("margin-left", "auto");
        buttonLayout.add(saveButton);
        layout.add(buttonLayout);

        return layout;
    }

}
