package hu.silverfrog.akasztofa.views.admin.builder;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.data.provider.DataProvider;
import hu.silverfrog.akasztofa.persistence.entity.GuessableWord;

import java.util.function.Consumer;

public class WordGridBuilder {
    private Consumer<GuessableWord> editAction;
    private DataProvider<GuessableWord, String> dataProvider;

    public WordGridBuilder withDataProvider(DataProvider<GuessableWord, String> dataProvider) {
        this.dataProvider = dataProvider;
        return this;
    }

    public WordGridBuilder withEditAction(Consumer<GuessableWord> editAction) {
        this.editAction = editAction;
        return this;
    }

    public Grid<GuessableWord> build() {
        Grid<GuessableWord> grid = new Grid<>();
        grid.addColumn(GuessableWord::getId).setHeader("ID").setSortable(true).setSortProperty("id");
        grid.addColumn(GuessableWord::getContent).setHeader("Szó").setSortable(true).setSortProperty("content");

        if (dataProvider != null) {
            grid.setDataProvider(dataProvider);
        }

        if (editAction != null) {
            grid.addComponentColumn(item -> {
                Button button = new Button("Szerkesztés");
                button.addClickListener(e -> editAction.accept(item));
                button.getStyle().set("background", "transparent");
                return button;
            });
        }
        return grid;
    }

}
