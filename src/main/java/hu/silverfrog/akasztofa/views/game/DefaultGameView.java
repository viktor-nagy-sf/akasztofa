package hu.silverfrog.akasztofa.views.game;

import com.flowingcode.vaadin.addons.simpletimer.SimpleTimer;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.textfield.TextField;
import hu.silverfrog.akasztofa.views.game.builder.GameLayoutBuilder;

import java.util.ArrayList;
import java.util.List;


public class DefaultGameView extends VerticalLayout implements GameView {
    private final H2 obfuscatedDisplay = new H2();
    private final TextField inputField = new TextField();
    private final ProgressBar progressBar = new ProgressBar();
    private final Label successMessage = new Label("Gratulálunk, sikeres megfejtés!");
    private final Label failureMessage = new Label("Sajnos ez most nem sikerült!");
    private final VerticalLayout inputLayout = new VerticalLayout();
    private final VerticalLayout badGuessLayout = new VerticalLayout();
    private final Text badGuessList = new Text("");
    private final SimpleTimer timer = new SimpleTimer();

    private final List<GameViewListener> listeners = new ArrayList<>();


    public DefaultGameView() {
        setAlignItems(Alignment.CENTER);
        timer.addTimerEndEvent(e -> handleTimeout());

        var layout = new GameLayoutBuilder()
                .withObfuscatedDisplay(obfuscatedDisplay)
                .withInputLayout(inputLayout)
                .withInputField(inputField)
                .withProgressBar(progressBar)
                .withSuccessMessage(successMessage)
                .withFailureMessage(failureMessage)
                .withInputLayout(inputLayout)
                .withBadGuessLayout(badGuessLayout)
                .withBadGuessList(badGuessList)
                .withResetAction(this::reset)
                .withProcessGuessAction(this::processGuess)
                .withCountdownClock(timer)
                .build();
        add(layout);
    }

    private void handleTimeout() {
        listeners.forEach(GameViewListener::handleTimeout);
    }

    private void reset() {
        listeners.forEach(GameViewListener::reset);
    }

    private void processGuess() {
        String guessText = inputField.getValue();
        if (guessText != null && !guessText.isEmpty()) {
            listeners.forEach(l -> l.processGuess(guessText));
        }
    }

    @Override
    public void setObfuscatedDisplay(String obfuscatedWord) {
        obfuscatedDisplay.setText(obfuscatedWord);
    }

    @Override
    public void setProgressBarValue(float value) {
        progressBar.setValue(value);
    }

    @Override
    public void toggleFailureMessageVisibility(boolean isVisible) {
        failureMessage.setVisible(isVisible);
    }

    @Override
    public void toggleSuccessMessage(boolean isVisible) {
        successMessage.setVisible(isVisible);
    }

    @Override
    public void toggleInputField(boolean isVisible) {
        inputLayout.setVisible(isVisible);
    }

    @Override
    public void setAlreadyGuessedInputErrorMessage() {
        inputField.setErrorMessage("A megadott karakter már volt használva!");
        inputField.setInvalid(true);
    }

    @Override
    public void setInvalidInputErrorMessage() {
        inputField.setErrorMessage("Csak 1 darab karakter adható meg!");
        inputField.setInvalid(true);
    }

    @Override
    public void clearInputErrorMessage() {
        inputField.setInvalid(false);
    }

    @Override
    public void clearInputField() {
        inputField.setValue("");
    }

    @Override
    public void setBadGuessList(String list) {
        badGuessLayout.setVisible(true);
        badGuessList.setText("Sikertelen tippek: " + list);
    }

    @Override
    public void clearBadGuessList() {
        badGuessLayout.setVisible(false);
    }

    @Override
    public void setupTimer(int timeoutSeconds) {
        timer.setStartTime(timeoutSeconds);
        timer.setFractions(false);
        timer.setMinutes(true);
    }

    @Override
    public void restartTimer() {
        timer.reset();
        if (!timer.isRunning()) {
            timer.start();
        }
    }

    @Override
    public void stopTimer() {
        timer.pause();
    }

    @Override
    public void displayTimeoutNotification(int timeoutSeconds) {
        new Notification(timeoutSeconds + " másodpercig nem tippeltél, növeljük a hibás tippek számát!", 3000).open();
    }


    @Override
    public void addListener(GameViewListener listener) {
        listeners.add(listener);
    }
}
