package hu.silverfrog.akasztofa.views.game;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import hu.silverfrog.akasztofa.services.GameService;

@Route("/game")
public class GameComponent extends VerticalLayout {

    public GameComponent(GameService gameService) {
        var model = new GameModel();
        var view = new DefaultGameView();

        GamePresenter presenter = new GamePresenter(view, model, gameService);
        presenter.reset();

        add(view);
    }
}
