package hu.silverfrog.akasztofa.views.game;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

import java.util.HashSet;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

@Data
public class GameModel {
    private static final Integer INVALID_GUESSES_ALLOWED = 5;

    private String selectedWord = "";
    private Set<Character> matchedChars = new HashSet<>();
    private Set<Character> unmatchedChars = new HashSet<>();
    @Getter(AccessLevel.NONE)
    private InputErrorMessage inputErrorMessage;
    private int timeoutTicks = 0;

    public String getObfuscatedWord() {
        return selectedWord.chars().map(c -> (matchedChars.contains((char) Character.toLowerCase(c)) ? c : '*')).collect(StringBuilder::new, (sb, c) -> sb.append((char)c), StringBuilder::append).toString();
    }

    private long getUniqueCharCount() {
        return selectedWord.toLowerCase(Locale.ROOT).chars().distinct().count();
    }

    public void resetWord(String word) {
        selectedWord = word;
        matchedChars = new HashSet<>();
        unmatchedChars = new HashSet<>();
        inputErrorMessage = null;
        timeoutTicks = 0;
    }

    public boolean isFinished() {
        return isLost() || isWon();
    }

    public boolean isWon() {
        return getUniqueCharCount() <= matchedChars.size();
    }

    public boolean isLost() {
        return getFailureCount() >= INVALID_GUESSES_ALLOWED;
    }

    public float getUnmatchedRate() {
        return (float) getFailureCount() / INVALID_GUESSES_ALLOWED;
    }

    public void incrementTimeoutTicks() {
        timeoutTicks++;
    }

    public void clearInputErrorMessage() {
        inputErrorMessage = null;
    }

    public Optional<InputErrorMessage> getInputErrorMessage() {
        return Optional.ofNullable(inputErrorMessage);
    }

    private int getFailureCount() {
        return unmatchedChars.size() + timeoutTicks;
    }

    public enum InputErrorMessage {
        ERROR_INVALID_INPUT,
        ERROR_ALREADY_GUESSED
    }
}
