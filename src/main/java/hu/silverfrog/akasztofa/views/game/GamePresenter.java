package hu.silverfrog.akasztofa.views.game;

import hu.silverfrog.akasztofa.services.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

public class GamePresenter implements GameView.GameViewListener {
    private final static Logger LOGGER = LoggerFactory.getLogger(GamePresenter.class);
    private final static int TIMEOUT_SECONDS = 30;

    private final Map<GameModel.InputErrorMessage, Runnable> errorMap;
    private final GameView view;
    private final GameModel model;
    private final GameService service;



    public GamePresenter(GameView view, GameModel model, GameService service) {
        this.view = view;
        this.model = model;
        this.service = service;
        view.addListener(this);
        view.setupTimer(TIMEOUT_SECONDS);

        errorMap = Map.ofEntries(
                Map.entry(GameModel.InputErrorMessage.ERROR_INVALID_INPUT, view::setInvalidInputErrorMessage),
                Map.entry(GameModel.InputErrorMessage.ERROR_ALREADY_GUESSED, view::setAlreadyGuessedInputErrorMessage)
        );
    }

    @Override
    public void reset() {
        model.resetWord(service.getRandomWord());
        view.clearInputField();
        updateView();
        view.restartTimer();
    }

    @Override
    public void processGuess(String guessText) {
        model.clearInputErrorMessage();
        if (guessText.length() > 1) {
            model.setInputErrorMessage(GameModel.InputErrorMessage.ERROR_INVALID_INPUT);
            updateView();
            return;
        }

        char c = guessText.toLowerCase(Locale.ROOT).charAt(0);

        if (model.getMatchedChars().contains(c) || model.getUnmatchedChars().contains(c)) {
            model.setInputErrorMessage(GameModel.InputErrorMessage.ERROR_ALREADY_GUESSED);
            updateView();
            return;
        }

        if (model.getSelectedWord().toLowerCase(Locale.ROOT).indexOf(c) >= 0) {
            model.getMatchedChars().add(c);
        } else {
            model.getUnmatchedChars().add(c);
        }
        view.clearInputField();
        if (!model.isFinished()) {
            view.restartTimer();
        }
        updateView();

    }

    @Override
    public void handleTimeout() {
        LOGGER.debug("handleTimeout running, model state: {}", model);
        model.incrementTimeoutTicks();
        model.clearInputErrorMessage();
        LOGGER.debug("handleTimeout UI access runs - model state: {}", model);
        view.displayTimeoutNotification(TIMEOUT_SECONDS);
        updateView();
        if (!model.isFinished()) {
            view.restartTimer();
        }
    }

    private void updateView() {
        view.setObfuscatedDisplay(model.isFinished() ? model.getSelectedWord() : model.getObfuscatedWord());
        view.setProgressBarValue(model.getUnmatchedRate());
        view.toggleFailureMessageVisibility(model.isLost());
        view.toggleSuccessMessage(model.isWon());
        view.toggleInputField(!model.isFinished());
        if (model.getInputErrorMessage().isPresent()) {
            errorMap.get(model.getInputErrorMessage().get()).run();
        } else {
            view.clearInputErrorMessage();
        }
        if (model.getUnmatchedChars().isEmpty()) {
            view.clearBadGuessList();
        } else {
            view.setBadGuessList(model.getUnmatchedChars().stream().map(String::valueOf).collect(Collectors.joining(", ")));
        }
    }
}
