package hu.silverfrog.akasztofa.views.game;

public interface GameView {
    void setObfuscatedDisplay(String obfuscatedWord);
    void setProgressBarValue(float value);
    void toggleFailureMessageVisibility(boolean isVisible);
    void toggleSuccessMessage(boolean isVisible);
    void toggleInputField(boolean isVisible);
    void setAlreadyGuessedInputErrorMessage();
    void setInvalidInputErrorMessage();
    void clearInputErrorMessage();
    void clearInputField();
    void setBadGuessList(String list);
    void clearBadGuessList();
    void setupTimer(int timeoutSeconds);
    void restartTimer();
    void stopTimer();
    void displayTimeoutNotification(int timeoutSeconds);

    void addListener(GameViewListener listener);

    interface GameViewListener {
        void reset();
        void processGuess(String guessText);
        void handleTimeout();
    }
}
