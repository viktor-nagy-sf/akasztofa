package hu.silverfrog.akasztofa.views.game.builder;

import com.flowingcode.vaadin.addons.simpletimer.SimpleTimer;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.textfield.TextField;

public class GameLayoutBuilder {
    private H2 obfuscatedDisplay;
    private TextField inputField;
    private ProgressBar progressBar;
    private Label successMessage;
    private Label failureMessage;
    private VerticalLayout inputLayout;
    private VerticalLayout badGuessLayout;
    private Text badGuessList;
    private Runnable resetAction;
    private Runnable processGuessAction;
    private SimpleTimer timer;

    public GameLayoutBuilder withCountdownClock(SimpleTimer timer) {
        this.timer = timer;
        return this;
    }

    public GameLayoutBuilder withObfuscatedDisplay(H2 obfuscatedDisplay) {
        this.obfuscatedDisplay = obfuscatedDisplay;
        return this;
    }

    public GameLayoutBuilder withInputField(TextField inputField) {
        this.inputField = inputField;
        return this;
    }

    public GameLayoutBuilder withProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
        return this;
    }

    public GameLayoutBuilder withSuccessMessage(Label successMessage) {
        this.successMessage = successMessage;
        return this;
    }

    public GameLayoutBuilder withFailureMessage(Label failureMessage) {
        this.failureMessage = failureMessage;
        return this;
    }

    public GameLayoutBuilder withInputLayout(VerticalLayout layout) {
        this.inputLayout = layout;
        return this;
    }

    public GameLayoutBuilder withBadGuessLayout(VerticalLayout layout) {
        this.badGuessLayout = layout;
        return this;
    }

    public GameLayoutBuilder withBadGuessList(Text badGuessList) {
        this.badGuessList = badGuessList;
        return this;
    }

    public GameLayoutBuilder withResetAction(Runnable action) {
        this.resetAction = action;
        return this;
    }

    public GameLayoutBuilder withProcessGuessAction(Runnable action) {
        this.processGuessAction = action;
        return this;
    }

    public VerticalLayout build() {
        var layout = new VerticalLayout();
        layout.setWidth("400px");
        layout.add(new Button("Új játék", e -> resetAction.run()));
        layout.add(successMessage);
        layout.add(failureMessage);

        var textLayout = new VerticalLayout();
        textLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        textLayout.add(obfuscatedDisplay);
        layout.add(textLayout);
        layout.add(progressBar);

        inputLayout.setWidthFull();
        inputLayout.setPadding(false);
        inputLayout.setSpacing(false);
        var timerLayout = new HorizontalLayout();
        timerLayout.add(new Span("Tippelésre hátralévő idő:"));
        timerLayout.add(timer);
        timerLayout.setWidthFull();
        inputLayout.add(timerLayout);

        var inputFieldLayout = new HorizontalLayout();
        inputFieldLayout.setWidthFull();
        inputField.setSizeFull();
        inputField.addKeyDownListener(Key.ENTER, e -> processGuessAction.run());
        inputFieldLayout.add(inputField);
        inputFieldLayout.add(new Button("Tipp", e -> processGuessAction.run()));
        inputLayout.add(inputFieldLayout);
        layout.add(inputLayout);


        badGuessLayout.setSizeFull();
        badGuessLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        badGuessLayout.add(badGuessList);
        badGuessLayout.setVisible(false);
        layout.add(badGuessLayout);

        return layout;
    }

}
